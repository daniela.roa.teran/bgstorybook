import { configure, addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withOptions } from '@storybook/addon-options';
import { withKnobs, select } from '@storybook/addon-knobs';
import { withViewport } from '@storybook/addon-viewport';
import React from 'react';
import themes from '../components/Theme/themes';
import ConfigStorybook from '../components/ConfigStorybook';
import Layout from '../components/Layout';

addDecorator(
  withOptions({
    name: `Storybook<br>${ConfigStorybook.version}`,
  })
);

addDecorator(withInfo({ inline: true, header: false }));

addDecorator(withKnobs);

addDecorator(withViewport);

addDecorator(story => {
  const content = story();

  const theme = select(
    'Theme',
    {
      Classic: themes.classic,
      Eminent: themes.eminent,
      Prefer: themes.prefer,
      Move: themes.move,
    },
    themes.classic
  );

  return (
    <Layout theme={theme} cdnBasepath="">
      {content}
    </Layout>
  );
});

// automatically import all files ending in *.stories.js
const req = require.context('../components', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
