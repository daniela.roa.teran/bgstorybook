import React, { Component, Fragment } from 'react';
import { withTheme } from '../Theme/ThemeContext';

class Button extends Component {
  render() {
    return (
      <Fragment>
        <button>{this.props.name}</button>

        <style jsx>{`
          button {
            background-color: ${this.props.theme.bright};
            border-radius: 4px;
            border: none;
            padding: 0 35px;
            height: 56px;
            font-size: 14px;
            color: white;
          }
        `}</style>
      </Fragment>
    );
  }
}

export default withTheme(Button);
