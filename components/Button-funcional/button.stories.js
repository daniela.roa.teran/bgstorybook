import React, { Fragment } from 'react';

import { storiesOf } from '@storybook/react';
import Button from './Button';
import markdown from './Button.md';
import Col from '../Col';
import Row from '../Row';

storiesOf('Componente funcional', module).add(
  'Default',
  () => (
    <Fragment>
      <Row>
        <Col md={12}>
          <Button name="BOTON FUNCIONAL" />
        </Col>
      </Row>
    </Fragment>
  ),
  { info: { text: markdown } }
);
