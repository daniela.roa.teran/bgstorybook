import React, { Fragment } from 'react';
import { withTheme } from '../Theme/ThemeContext';

const Button = ({ name, theme }) => {
  return (
    <Fragment>
      <button>{name}</button>
      <style jsx>{`
        button {
          background-color: ${theme.bright};
          border-radius: 4px;
          border: none;
          padding: 0 35px;
          height: 56px;
          font-size: 14px;
          color: white;
        }
      `}</style>
    </Fragment>
  );
};

export default withTheme(Button);
