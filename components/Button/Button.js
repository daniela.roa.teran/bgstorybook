import React, { Fragment } from 'react';
import { withTheme } from '../Theme/ThemeContext';
import classNames from 'classnames';
import filter from '../filter-props';

const Button = ({
  size,
  bgColor,
  color,
  isDisabled,
  arrowRight,
  arrowLeft,
  backMobile,
  children,
  theme,
  className,
  bgColorGrow,
  ...rest
}) => {
  const filteredProps = filter(rest);

  return (
    <Fragment>
      <button
        {...filteredProps}
        disabled={isDisabled}
        className={classNames(className)}
      >
        <span>{children}</span>
      </button>
      <style jsx>{`
        button {
          background-color: ${isDisabled
            ? theme.gray30
            : bgColor === false
            ? 'transparent'
            : bgColor === 'gray'
            ? theme.gray30
            : color === 'gray'
            ? 'transparent'
            : theme.bright};
          color: ${isDisabled
            ? theme.gray60
            : bgColor === false
            ? theme.bright
            : bgColor === 'gray'
            ? theme.gray
            : color === 'gray'
            ? theme.gray60
            : 'white'};
          font-size: ${size === 'lg' ? theme.buttonLg : theme.buttonSm};
          border-radius: 4px;
          border: none;
          text-transform: uppercase;
          cursor: ${bgColor === 'gray' ? 'no-drop' : 'pointer'};
          transition: ${theme.animateButton};
          padding: ${bgColor === false || color === 'gray'
            ? '0px'
            : size === 'lg'
            ? '0 35px'
            : '0 16px'};

          height: ${size === 'lg' ? '56px;' : '36px'};
          position: relative;
          z-index: 1;
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          font-weight: 400;
          line-height: initial;
        }
        button:hover {
          background-color: ${isDisabled
            ? theme.gray10
            : bgColor === false
            ? 'transparent'
            : bgColor === 'gray'
            ? theme.gray30
            : color === 'gray'
            ? 'transparent'
            : theme.deep};
          color: ${isDisabled
            ? theme.gray60
            : bgColor === false
            ? theme.deep
            : bgColor === 'gray'
            ? theme.gray60
            : color === 'gray'
            ? theme.gray80
            : theme.white};
          transition: ${theme.animateButton};
          cursor: ${isDisabled ? 'default' : 'pointer'};
        }
        button:focus {
          outline: none;
        }
        button:after {
          font-family: 'galicia-ui';
          content: '\\e906';
          display: ${arrowRight === true ? 'inline-block' : 'none'};
          position: relative;
          top: 0;
          margin-left: 5px;
          font-size: 16px;
        }
        button:before {
          font-family: 'galicia-ui';
          content: '\\e902';
          display: ${arrowLeft === true ? 'inline-block' : 'none'};
          position: relative;
          top: ${color === 'gray' ? '2px' : '0'};
          margin-right: 5px;
          font-size: ${color === 'gray' ? '20px' : '16px'};
        }
        button span {
          position: relative;
          top: ${arrowRight === true || arrowLeft === true ? '-2px' : '0px'};
        }
        @media screen and (max-width: 768px) {
          button span {
            display: ${backMobile ? 'none' : 'inline-block'};
            font-size: ${backMobile ? '18px' : ''};
          }
          button:before {
            font-size: ${backMobile ? '18px' : '12px'};
          }
        }
      `}</style>
    </Fragment>
  );
};

export default withTheme(Button);
