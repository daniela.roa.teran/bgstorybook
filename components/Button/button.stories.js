import React, { Fragment } from 'react';

import { storiesOf } from '@storybook/react';
import Button from './Button';
import markdown from './Button.md';
import Col from '../Col';
import Row from '../Row';

storiesOf('Buttons', module)
  .add(
    'Button',
    () => (
      <div>
        <Row>
          <Col md={12}>
            <Button size="sm">NUEVO PRESTAMO</Button>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col md={12}>
            <Button size="lg">NUEVO PRESTAMO</Button>
          </Col>
        </Row>
      </div>
    ),
    { info: { text: markdown } }
  )
  .add(
    'Button arrow',
    () => (
      <div>
        <Row>
          <Col md={12}>
            <Button size="sm" arrowLeft={true}>
              NUEVO PRESTAMO
            </Button>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col md={12}>
            <Button size="lg" arrowRight={true}>
              NUEVO PRESTAMO
            </Button>
          </Col>
        </Row>
      </div>
    ),
    { info: { text: markdown } }
  )

  .add(
    'Button disabled',
    () => (
      <div>
        <Row>
          <Col md={12}>
            <Button size="sm" isDisabled={true}>
              NUEVO PRESTAMO
            </Button>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col md={12}>
            <Button bgColor="gray" size="lg" isDisabled={true}>
              NUEVO PRESTAMO
            </Button>
          </Col>
        </Row>
      </div>
    ),
    { info: { text: markdown } }
  )

  .add(
    'Button back',
    () => (
      <Button color="gray" arrowLeft={true} size="sm">
        VOLVER
      </Button>
    ),
    { info: { text: markdown } }
  )

  .add(
    'Button link',
    () => (
      <Fragment>
        <Row>
          <Col md={2}>
            <Button bgColor={false} size="lg">
              NUEVO PRESTAMO
            </Button>
          </Col>
        </Row>
      </Fragment>
    ),
    {
      info: { text: markdown },
    }
  );
