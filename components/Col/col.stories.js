import React from 'react';

import { storiesOf } from '@storybook/react';
import Col from './Col';

storiesOf('Layout/Grid', module).add('Col', () => <Col />);
