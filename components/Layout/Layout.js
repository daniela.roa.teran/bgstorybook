import React, { Component } from 'react';
import 'bootstrap-4-grid/css/grid.css';
import Row from '../Row/Row';
import Col from '../Col/Col';
import ThemeContext from '../Theme/ThemeContext';

class Layout extends Component {
  render() {
    const { children, theme, cdnBasepath } = this.props;

    return (
      <ThemeContext.Provider
        value={{
          theme,
          cdnBasepath,
        }}
      >
        <Row>
          <Col md={12}>{children}</Col>
        </Row>

        <style jsx global>{`
           {
            /* reset styles */
          }

          div,
          span,
          applet,
          object,
          iframe,
          h1,
          h2,
          h3,
          h4,
          h5,
          h6,
          p,
          blockquote,
          pre,
          a,
          abbr,
          acronym,
          address,
          big,
          cite,
          code,
          del,
          dfn,
          em,
          img,
          ins,
          kbd,
          q,
          s,
          samp,
          small,
          strike,
          strong,
          sub,
          sup,
          tt,
          var,
          b,
          u,
          i,
          center,
          dl,
          dt,
          dd,
          ol,
          ul,
          li,
          fieldset,
          form,
          label,
          legend,
          table,
          caption,
          tbody,
          tfoot,
          thead,
          tr,
          th,
          td,
          article,
          aside,
          canvas,
          details,
          embed,
          figure,
          figcaption,
          footer,
          header,
          hgroup,
          menu,
          nav,
          output,
          ruby,
          section,
          summary,
          time,
          mark,
          audio,
          video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
          }
           {
            /* reset styles */
          }

          @font-face {
            font-family: 'Glyphicons Halflings';
            src: url(${cdnBasepath}/fonts/glyphicons-halflings-regular.eot);
            src: url(${cdnBasepath}/fonts/glyphicons-halflings-regular.eot?#iefix)
                format('embedded-opentype'),
              url(${cdnBasepath}/fonts/glyphicons-halflings-regular.woff2)
                format('woff2'),
              url(${cdnBasepath}/fonts/glyphicons-halflings-regular.woff})
                format('woff'),
              url(${cdnBasepath}/fonts/glyphicons-halflings-regular.ttf})
                format('truetype'),
              url(${cdnBasepath}/fonts/glyphicons-halflings-regular.svg})
                format('svg');
            font-display: swap;
          }

          @font-face {
            font-family: 'galicia-ui';
            src: url(${cdnBasepath}/fonts/storybook-ui.eot);
            src: url(${cdnBasepath}/fonts/storybook-ui.eot#iefix)
                format('embedded-opentype'),
              url(${cdnBasepath}/fonts/storybook-ui.ttf) format('truetype'),
              url(${cdnBasepath}/fonts/storybook-ui.woff) format('woff'),
              url(${cdnBasepath}/fonts/storybook-ui.svg) format('svg');
            font-weight: normal;
            font-style: normal;
            font-display: swap;
          }

          html {
            position: relative;
            min-height: 100%;
          }
          body {
            overflow-x: hidden;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          }
        `}</style>
      </ThemeContext.Provider>
    );
  }
}

export default Layout;
