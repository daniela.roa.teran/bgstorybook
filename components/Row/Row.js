import classNames from 'classnames';
import PropTypes from 'prop-types';

import React from 'react';

class Row extends React.Component {
  static propTypes = {
    /**
     * @default 'row'
     */
    bsPrefix: PropTypes.string.isRequired,

    /** Removes the gutter spacing between `Col`s as well as any added negative margins. */
    noGutters: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    as: 'div',
    noGutters: false,
    bsPrefix: 'row',
  };

  render() {
    const {
      bsPrefix,
      noGutters,
      as: Component,
      className,
      ...props
    } = this.props;

    return (
      <Component
        {...props}
        className={classNames(className, bsPrefix, noGutters && 'no-gutters')}
      />
    );
  }
}

export default Row;
