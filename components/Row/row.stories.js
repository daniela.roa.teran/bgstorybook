import React from 'react';

import { storiesOf } from '@storybook/react';
import Row from './Row';

storiesOf('Layout/Grid', module).add('Row', () => <Row />);
