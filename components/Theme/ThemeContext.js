import React from 'react';
import themes from './themes';

const ThemeContext = React.createContext({
  theme: themes.classic,
  cdnBasepath: '',
});

export function withTheme(Component) {
  const ThemedComponent = props => {
    return (
      <ThemeContext.Consumer>
        {context => (
          <Component
            {...props}
            cdnBasepath={context.cdnBasepath}
            theme={context.theme}
          />
        )}
      </ThemeContext.Consumer>
    );
  };

  ThemedComponent.displayName = Component.displayName || Component.name;

  return ThemedComponent;
}

export default ThemeContext;
