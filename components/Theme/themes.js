const colors = {
  red: '#e21414',
  green: '#36a04c',
  blue: '#215a97',
  yellow: '#e6af13',
  white: '#FFFFFF',
  gray: '#e1e3e6',
  black: '#292b2f', //title black && texts
  white70: 'rgba(255, 255, 255, 0.7)',
  gray80: '#8e8e8f',
  gray60: '#a7a7a9',
  gray40: '#dbdbdb',
  gray30: '#f4f4f5',

  black60: '#111214',
  black40: '#41454A',
  black50: '#292b2f',
  black30: '#595e65',
  black20: '#717780', //title gray && texts

  orange65: '#af4600',
  orange60: '#c85000',
  orange55: '#e15a00',
  orange50: '#fa6400',
  orange40: '#ff822e',
  orange20: '#ffbf94',
  orange10: '#ffdec7',

  lighterOrange: '#fa6400',
  darkerOrange: '#d6410f',
  brightOrange: '#ff6600',
  deepOrange: '#eb5600',
  darkOrange: '#e64d00',
  alphaOrange: '#ffece0',

  lighterGold: '#d5bc7c',
  darkerGold: '#937938',
  brightGold: '#c0a664',
  deepGold: '#af9348',
  darkGold: '#997e39',
  alphaGold: '#f1e3c1',

  lighterLightBlue: '#0b9bd9',
  darkerLightBlue: '#0077b8',
  brightLightBlue: '#03a9f4',
  deepLightBlue: '#0a86bf',
  darkLightBlue: '#0077b8',
  alphaLightBlue: '#e2f5fd',

  lightBlueNasty: '#71be44',
  green20: '#bae8c4',

  lighterBlue: '#003973',
  darkerBlue: '#00264d',
  brightBlue: '#003973',
  deepBlue: '#003061',
  darkBlue: '#00264d',
  alphaBlue: '#d5dce3',

  eminent60: '#002041',
  eminent55: '#002c5a',
  eminent50: '#003973',
  eminent40: '#0052a6',
};

const fonts = {
  //font size
  px10size: '10px',
  px12size: '12px',
  px14size: '14px',
  px16size: '16px',
  px18size: '18px',
  px20size: '20px',
  px22size: '22px',
  px24size: '24px',
  px26size: '26px',
  px28size: '28px',
  px30size: '30px',
  px32size: '32px',
  px48size: '48px',
  px64size: '64px',
  px128size: '128px',
};

const size = {
  px120size: '120px',
};

const animate = {
  animate150ms: '150ms ease-in-out',
  animate170ms: '170ms ease-in-out',
  animate200ms: '200ms ease-in-out',
  animate350ms: '350ms ease-out',
};

const separation = {
  space4: '4px',
  space8: '8px',
  space16: '16px',
  space24: '24px',
  space32: '32px',
  space48: '48px',
  space95: '95px',

  spaceRight4: '0 4px 0 0',
  spaceRight8: '0 8px 0 0',
  spaceRight16: '0 16px 0 0',
  spaceRight24: '0 24px 0 0',
  spaceRight32: '0 32px 0 0',
  spaceRight48: '0 48px 0 0',
  spaceLeft4: '0 0 0 4px',
  spaceLeft8: '0 0 0 8px',
  spaceLeft16: '0 0 0 16px',
  spaceLeft24: '0 0 0 24px',
  spaceLeft32: '0 0 0 32px',
  spaceLeft48: '0 0 0 48px',

  spaceTop4: '4px 0 0 0',
  spaceTop8: '8px 0 0 0',
  spaceTop16: '16px 0 0 0',
  spaceTop24: '24px 0 0 0',
  spaceTop32: '32px 0 0 0',
  spaceTop48: '48px 0 0 0',
  spaceBottom4: '0 0 4px 0',
  spaceBottom8: '0 0 8px 0',
  spaceBottom16: '0 0 16px',
  spaceBottom24: '0 0 24px',
  spaceBottom32: '0 0 32px',
  spaceBottom48: '0 0 48px',

  spaceVertical4: '4px 0',
  spaceVertical8: '8px 0',
  spaceVertical16: '16px 0',
  spaceVertical24: '24px 0',
  spaceVertical32: '32px 0',
  spaceVertical48: '48px 0',
  spaceHorizontal16: '0 16px',
  spaceHorizontal8: '0 8px',
};

const helpers = {
  left: 'left',
  right: 'right',
};

const classic = {
  bright: colors.brightOrange,
  deep: colors.deepOrange,
  dark: colors.darkOrange,
  light: colors.orange10,
};

const eminent = {
  bright: colors.eminent50,
  deep: colors.eminent55,
  dark: colors.eminent60,
  light: colors.eminent40,
};

const prefer = {
  bright: colors.brightGold,
  deep: colors.deepGold,
  dark: colors.darkGold,
  light: colors.alphaGold,
};

const move = {
  bright: colors.brightLightBlue,
  deep: colors.deepLightBlue,
  dark: colors.darkLightBlue,
  light: colors.alphaLightBlue,
  nasty: colors.lightBlueNasty,
};

const base = {
  //font size names
  H1Text: fonts.px32size,
  H2Text: fonts.px30size,
  H3Text: fonts.px24size,
  H4Text: fonts.px20size,
  H5Text: fonts.px16size,
  Paragraph: fonts.px16size,
  ParagraphSm: fonts.px12size,
  ParagraphMd: fonts.px14size,
  spamMd: fonts.px16size,
  inputLabelFont: fonts.px16size,
  inputLabel: fonts.px12size,

  H1TextMob: fonts.px28size,
  H2TextMob: fonts.px26size,
  H3TextMob: fonts.px30size,
  H4TextMob: fonts.px26size,
  H5TextMob: fonts.px24size,

  layoutTitlemdMob: fonts.px22size,

  paragraphCardSm: fonts.px12size,
  paragraphCard: fonts.px14size,
  titleSmCard: fonts.px16size,
  titleMdCard: fonts.px24size,
  buttonSm: fonts.px12size,
  buttonLg: fonts.px14size,
  iconSize: fonts.px22size,
  titleSideDrawer: fonts.px24size,
  sideDrawerText: fonts.px14size,
  modalTitleFont: fonts.px16size,
  modalBodyFont: fonts.px14size,
  modalFooterFont: fonts.px12size,

  //Colors
  gray80: colors.gray80,
  gray60: colors.gray60,
  gray40: colors.gray40,
  gray30: colors.gray30,

  black60: colors.black60,
  black40: colors.black40,
  black30: colors.black30,
  black20: colors.black20,

  white: colors.white,
  gray: colors.gray,
  green: colors.green,
  black: colors.black,
  blackdark: colors.black60,
  red: colors.red,
  yellow: colors.yellow,
  green20: colors.green20,
  white70: colors.white70,

  buttonColorHover: colors.gray10,
  buttonMobileDrop: colors.gray,
  cardBgGray: colors.gray30,
  darkerOrange: colors.darkerOrange,

  //Global variable
  heightCard: size.px120size,

  //Global animate
  animateGlobal: animate.animate200ms,
  animateButton: animate.animate200ms,
  animateCard: animate.animate200ms,
  animateSideDrawer: animate.animate200ms,
  animateLoadingOut: animate.animate350ms,
  //Full separation
  grow4: separation.space4,
  grow8: separation.space8,
  grow16: separation.space16,
  grow24: separation.space24,
  grow32: separation.space32,
  grow48: separation.space48,
  grow95: separation.space95,
  //vertical separation
  growVertical4: separation.spaceVertical4,
  growVertical8: separation.spaceVertical8,
  growVertical16: separation.spaceVertical16,
  growVertical24: separation.spaceVertical24,
  growVertical32: separation.spaceVertical32,
  growVertical48: separation.spaceVertical48,
  // Horizontal separation
  growHorizontal4: separation.spaceHorizontal4,
  growHorizontal8: separation.spaceHorizontal8,
  growHorizontal16: separation.spaceHorizontal16,
  growHorizontal24: separation.spaceHorizontal24,
  growHorizontal32: separation.spaceHorizontal32,
  growHorizontal48: separation.spaceHorizontal48,
  //Right separation
  growRigth4: separation.spaceRight4,
  growRigth8: separation.spaceRight8,
  growRigth16: separation.spaceRight16,
  growRigth24: separation.spaceRight24,
  growRigth32: separation.spaceRight32,
  growRigth48: separation.spaceRight48,
  //Left separation
  growLeft4: separation.spaceLeft4,
  growLeft8: separation.spaceLeft8,
  growLeft16: separation.spaceLeft16,
  growLeft24: separation.spaceLeft24,
  growLeft32: separation.spaceLeft32,
  growLeft48: separation.spaceLeft48,
  //Top separation
  growTop4: separation.spaceTop4,
  growTop8: separation.spaceTop8,
  growTop16: separation.spaceTop16,
  growTop24: separation.spaceTop24,
  growTop32: separation.spaceTop32,
  growTop48: separation.spaceTop48,
  //Bottom separation
  growBottom4: separation.spaceBottom4,
  growBottom8: separation.spaceBottom8,
  growBottom16: separation.spaceBottom16,
  growBottom24: separation.spaceBottom24,
  growBottom32: separation.spaceBottom32,
  growBottom48: separation.spaceBottom48,

  //Icons size
  iconXs: fonts.px16size,
  iconSm: fonts.px20size,
  iconMd: fonts.px24size,
  iconLg: fonts.px32size,
  iconXl: fonts.px48size,
  iconXXL: fonts.px64size,
  iconXXXL: fonts.px128size,
  //Icon colors
  error: colors.red,
  success: colors.green,

  //Helpers
  toLeft: helpers.left,
  toRight: helpers.right,

  //inputs
  inputSm: fonts.px16size,
  inputMd: fonts.px32size,
  inputLg: fonts.px48size,
  inputXl: fonts.px64size,
  errorLabelSize: fonts.px10size,

  borderRadius: separation.space4,
};

export default {
  classic: { ...base, ...classic },
  eminent: { ...base, ...eminent },
  prefer: { ...base, ...prefer },
  move: { ...base, ...move },
};
