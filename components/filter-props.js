function filterProps(props) {
  const copy = { ...props };
  delete copy.cdnBasepath;

  return copy;
}

export default filterProps;
