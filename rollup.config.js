import babel from 'rollup-plugin-babel';
import pkg from './package.json';
import nodeResolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';
import url from 'rollup-plugin-url';
import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';
import postCssPresetEnv from 'postcss-preset-env';
import cssNano from 'cssnano';

const NODE_ENV = process.env.NODE_ENV || 'development';
const outputFile =
  NODE_ENV === 'production' ? './dist/index.prod.js' : './dist/index.dev.js';

const urlPlugin = url({
  limit: 10 * 1024, // inline files < 10k, copy files > 10k
  include: [
    '**/*.svg',
    '**/*.png',
    '**/*.jpg',
    '**/*.gif',
    '**/*.eot',
    '**/*.woff',
    '**/*.woff2',
    '**/*.ttf',
    '**/*.md',
  ],
  fileName: '[dirname][name][extname]',
});

const babelOptions = {
  exclude: 'node_modules/**',
};

export default {
  input: 'components/index.js',
  output: [
    {
      file: outputFile,
      format: 'cjs',
    },
    {
      file: pkg.module,
      format: 'es',
    },
  ],
  external: id => /^react|styled-jsx/.test(id),
  plugins: [
    replace({ 'process.env.NODE_ENV': JSON.stringify('NODE_ENV') }),
    babel(babelOptions),
    nodeResolve(),
    commonjs(),
    postcss({ plugins: [postCssPresetEnv, cssNano] }),
    urlPlugin,
    // terser(),
  ],
};
